// Import modules
require('dotenv').config();

const express = require('express');
const app = express();
const mongoose = require('mongoose');
const cors = require('cors');

// Connect to the database 
// mongodb default port: 27017
mongoose.connect(process.env.DATABASE_URL, {  
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    useUnifiedTopology: true 
});

const db = mongoose.connection;
// Always listen on database connection error
db.on('error', (error) => console.error(error));
// Once connected to the database
db.once('open', () => console.log(`Connected to Database at ${db.host}`));

// Body Parser
app.use(express.json());
// Cross-origin resource sharing
app.use(cors());

// Route file
const studentsRouter = require('./routes/students');
const majorsRouter = require('./routes/majors');

// Mount routers
app.use('/students', studentsRouter);
app.use('/majors', majorsRouter);

// Server run at port...
app.listen(process.env.PORT, () => console.log(`Server Started at Port: ${process.env.PORT}`));
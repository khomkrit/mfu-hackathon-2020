

###
// get all students
GET http://localhost:3000/students

###
// get one student from id
GET http://localhost:3000/students/5fb52fd1c4d0c83a841e8db8

###
// create student
POST http://localhost:3000/students
Content-Type: application/json

{
    "firstName": "John",
    "lastName": "Doe",
    "studentId": "0000000000",
    "major": "5fb54480984f222f10c608cf"
}

###
// update student
PUT  http://localhost:3000/students/5fb5448d984f222f10c608d7
Content-Type: application/json

{
    "firstName": "Sarah"
}

###
// delete one student from id
DELETE  http://localhost:3000/students/5fb52fd1c4d0c83a841e8db7

###
// get all majors
GET http://localhost:3000/majors

###
// get one major from id
GET http://localhost:3000/majors/5fb52fd1c4d0c83a841e8db8

###
// create major
POST http://localhost:3000/majors
Content-Type: application/json

{
    "name": "Software Engineering"
}

###
// update one major from id
PUT http://localhost:3000/majors/5fb52f605637b369f8150cc2
Content-Type: application/json

{
    "name": "Medicine"
}

###
// delete one major from id
DELETE  http://localhost:3000/majors/5fb53ea6f34e77656c8452ce

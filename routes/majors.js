const express = require('express');
const router = express.Router();
const Major = require('../models/major');

// @desc Getting all majors
// @route GET /majors
router.get('/', async (req, res) => {
  try {
    const majors = await Major.find();
    res.status(200).json({ data: majors });
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
});

// Validate major id (middleware function)
async function getMajor(req, res, next) {
  let major;
  try {
    major = await Major.findById(req.params.id);
    if (!major) {
      return res.status(404).json({ message: 'Cannot find major' });
    }
  } catch (err) {
    // Bad ObjectId
    let message = err.message;
    let code = 500;
    if (err.name === 'CastError') {
      message = 'Resource not found';
      code = 404;
    }
    return res.status(code).json({ message });
  }

  req.major = major;
  next();
}

// @desc Getting one major
// @route GET /majors/:id
router.get('/:id', getMajor, (req, res) => {
  res.status(200).json({ data: req.student });
});

// @desc Creating one major
// @route POST /majors
router.post('/', async (req, res) => {
  try {
    const newMajor = await Major.create(req.body);
    res.status(201).json({ data: newMajor });
  } catch (err) {
    if (err.code === 11000) {
      const keys = Object.keys(err.keyValue);
      err.message = `This ${keys.toString()} is already taken.`;
    }
    res.status(400).json({ message: err.message });
  }
});

// @desc Updating one major
// @route PUT /majors/:id
router.put('/:id', getMajor, async (req, res) => {
  const majorId = req.params.id;
  try {
    const updatedMajor = await Major.findByIdAndUpdate(majorId, req.body, {
      new: true,
      runValidators: true,
    });
    res.json({ data: updatedMajor });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
});

// @desc Delete one major
// @route DELETE /majors/:id
router.delete('/:id', getMajor, async (req, res) => {
  const major = req.major;
  try {
    await major.remove();
    res.json({ message: 'Deleted Major', data: {} });
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
});

module.exports = router;

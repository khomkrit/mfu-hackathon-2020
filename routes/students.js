const express = require('express');
const router = express.Router();
const Student = require('../models/student');

// @desc Getting all students
// @route GET /students
router.get('/', async (req, res) => {
  try {
    const students = await Student.find().populate({ path: 'major' });
    res.status(200).json({ data: students });
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
});

// Validate student id (middleware function)
async function getStudent(req, res, next) {
  let student;
  try {
    student = await Student.findById(req.params.id).populate({ path: 'major' });
    if (!student) {
      return res.status(404).json({ message: 'Cannot find student' });
    }
  } catch (err) {
    // Bad ObjectId
    let message = err.message;
    let code = 500;
    if (err.name === 'CastError') {
      message = 'Resource not found';
      code = 404;
    }
    return res.status(code).json({ message });
  }
  req.student = student;
  next();
}

// @desc Getting one students
// @route GET /students/:id
router.get('/:id', getStudent, (req, res) => {
  res.status(200).json({ data: req.student });
});

// @desc Creating one student
// @route POST /students/:id
router.post('/', async (req, res) => {
  try {
    const newStudent = await Student.create(req.body);
    res.status(201).json({ data: newStudent });
  } catch (err) {
    if (err.code === 11000) {
      const keys = Object.keys(err.keyValue);
      err.message = `This ${keys.toString()} is already taken.`;
    }
    res.status(400).json({ message: err.message });
  }
});

// @desc Updating one student
// @route PUT /students/:id
router.put('/:id',getStudent, async (req, res) => {
  const studentId = req.params.id;
  try {
    const updatedStudent = await Student.findByIdAndUpdate(studentId, req.body, {
      new: true,
      runValidators: true,
    });
    res.json({ data: updatedStudent });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
});

// @desc Deleting one student
// @route DELETE /students/:id
router.delete('/:id', getStudent, async (req, res) => {
  const student = req.student;
  try {
    await student.remove();
    res.json({ message: 'Deleted Student', data: {} });
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
});

module.exports = router;

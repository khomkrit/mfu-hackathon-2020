# MFU Hackathon 2020

> Basic Express.js with MongoDB

## Installation

``` bash
# install dependencies
$ npm install

``` 
# Run App
``` bash
# Run in development mode
$ npm run dev

# Run in production mode
$ npm start

